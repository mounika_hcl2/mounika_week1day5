package day_5demo;


	import java.util.Scanner;
	class ReverseFinder{
		public static void findReverse(int number){

			if(number<10) {
				System.out.println(number);
				return;
			}
			else {
				System.out.print(number%10);
				findReverse(number/10);
			}

		}
		public static void main(String[] args) {
			System.out.println("Enter no:");

			Scanner in = new Scanner(System.in);

			int num = in.nextInt();

			System.out.println();
			findReverse(num);
			in.close();

		}

	}



}
